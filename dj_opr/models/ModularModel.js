/**
 * Created by yishuangxi on 2016/1/11.
 */
var swig = require('swig');
var db = require('../lib/database');
var async = require('async');

var Modular = function (data) {
};

Modular.prototype.create = function (name, title, url, file, script, state, callback) {
    var sql = 'insert into modular values (null, "{{name}}", "{{title}}", "{{url}}", "{{file}}", "{{script}}", {{state}}, "{{createTime}}")';
    sql = swig.compile(sql)({
        name: name,
        title: title,
        url: url,
        file: file,
        script: script,
        state: state,
        createTime: parseInt(Date.now() / 1000)
    });
    db.query(sql, callback);
};

Modular.prototype.findById = function (id, callback) {
    var sql = 'select id, name, state, createTime, ' +
        '(select modularId from modular_modular where modularId=modular.id ) as modularId, ' +
        '(select name from modular where modularId=modular.id ) as modularId, ' +
        'from modular where id=' + id;
    db.query(sql, callback);
};

Modular.prototype.find = function (name, title, state, callback) {
    var sql = 'select id, name, title, url, file, script, state, createTime from modular where name like "%{{name}}%" and title like "%{{title}}%" ';
    if(state === 1 || state === 0){
        sql += 'and state={{state}} '
    }
    sql+= 'order by createTime desc ';
    sql = swig.compile(sql)({
        name: name,
        title:title,
        state:state
    });
    db.query(sql, callback);
};

Modular.prototype.update = function (id, title, url, file, script, state, callback) {
    var sql = 'update modular set title="{{title}}",url="{{url}}",file="{{file}}",script="{{script}}", state={{state}} where id={{id}}';
    sql = swig.compile(sql)({
        id: id,
        title: title,
        url: url,
        file: file,
        script: script,
        state: state
    });
    db.query(sql, callback);
};

Modular.prototype.password = function (id, callback) {
    var sql = 'update modular set password={{password}} where id={{id}}';
    sql = swig.compile(sql)({password: '88888888', id: id});
    db.query(sql, callback);
};

module.exports = Modular;

/*
 * Model API HTTP Method Example Path

 create()

 POST

 /locations

 upsert()

 PUT

 /locations

 exists()

 GET

 /locations/:id/exists

 findById()

 GET

 /locations/:id

 find()

 GET

 /locations

 findOne()

 GET

 /locations/findOne

 deleteById()

 DELETE

 /locations/:id

 count()

 GET

 /locations/count

 prototype.updateAttributes()

 PUT

 /locations/:id


 * */

