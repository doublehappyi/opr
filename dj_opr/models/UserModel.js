/**
 * Created by yishuangxi on 2016/1/11.
 */
var swig = require('swig');
var db = require('../lib/database');
var async = require('async');

var User = function (data) {
};

User.prototype.create = function (username, roleId, state, callback) {
    var sql = 'insert into user values (null, "{{username}}", "{{password}}", {{state}}, {{createTime}}, {{roleId}})';
    sql = swig.compile(sql)({
        username: username,
        password: '88888888',
        state: state,
        createTime: parseInt(Date.now() / 1000),
        roleId: roleId
    });
    db.query(sql, callback);
};

User.prototype.findById = function (id, callback) {
    var sql = 'select id, username, state, createTime, ' +
        '(select roleId from user_role where userId=user.id ) as roleId, ' +
        '(select name from role where userId=user.id ) as roleId, ' +
        'from user where id=' + id;
    db.query(sql, callback);
};

User.prototype.find = function (username, roleId, state, page, callback) {
    var pageNum = 30;
    var conLimit = 'limit ' + pageNum + ' offset ' + parseInt(page - 1) * pageNum;
    var conWhere = 'where username like "%{{username}}%" ';
    if (roleId) {
        conWhere += 'and  roleId={{roleId}} ';
    }
    if (state) {
        conWhere += 'and state={{state}} '
    }
    conWhere = swig.compile(conWhere)({
        username: username,
        roleId: roleId,
        state: state
    });
    var conPageSelect = 'select id, username, state, createTime, roleId, ' +
        '(select name from role where id=user.roleId ) as roleName ';
    var conFrom = 'from user ';
    var conCountSelect = 'select count(id) as count ';

    var sqlPage = conPageSelect + conFrom + conWhere +' order by createTime desc '+ conLimit ;
    var sqlCount = conCountSelect + conFrom + conWhere;
    async.parallel({
        count: function (cb) {
            db.query(sqlCount, function(err, rows, fields){
                cb(err, rows);
            });
        },
        rows: function (cb) {
            db.query(sqlPage, function(err, rows, fields){
                cb(err, rows);
            });
        }
    }, function (err, results) {
        callback(err, results.count, results.rows);
    });
};

User.prototype.update = function (id, roleId, state, callback) {
    var sql = 'update user set state={{state}}, roleId={{roleId}} where id={{id}}';
    sql = swig.compile(sql)({state: state, roleId: roleId, id: id});
    db.query(sql, callback);
};

User.prototype.password = function (id, callback) {
    var sql = 'update user set password={{password}} where id={{id}}';
    sql = swig.compile(sql)({password: '88888888', id: id});
    db.query(sql, callback);
};

module.exports = User;

/*
 * Model API HTTP Method Example Path

 create()

 POST

 /locations

 upsert()

 PUT

 /locations

 exists()

 GET

 /locations/:id/exists

 findById()

 GET

 /locations/:id

 find()

 GET

 /locations

 findOne()

 GET

 /locations/findOne

 deleteById()

 DELETE

 /locations/:id

 count()

 GET

 /locations/count

 prototype.updateAttributes()

 PUT

 /locations/:id


 * */

