/**
 * Created by yishuangxi on 2016/1/11.
 */
var swig = require('swig');
var db = require('../lib/database');
var async = require('async');
var dateformat = require('dateformat');
var Hotclass = function (data) {
};

Hotclass.prototype.save = function (keyword, rows, expireTime, state, callback) {
    var values = [];
    var valueStr = '("{{keyword}}", "{{hotclassId}}", "{{hotclassName}}", {{score}},"{{expireTime}}", {{state}})';
    if (rows.length > 0) {
        for (var i = 0; i < rows.length; i++) {
            values.push(swig.compile(valueStr)(rows[i]));
        }
    }

    //删除hotclass表中所有keyword=keyword的数据
    var sql_delete = 'delete from hotclass where keyword="' + keyword + '"';
    //如果keyword不存在，则插入
    var sql_insert = 'insert into hotclass values ' + values.join(',');
    console.log('sql_insert: ', sql_insert);
    db.pool.getConnection(function (err, conn) {
        if (err) {
            callback(err)
        }
        conn.beginTransaction(function (err) {
            if (err) {
                callback(err)
            }
            conn.query(sql_delete, function (err, result) {
                if (err) {
                    return conn.rollback(function (err) {
                        callback(err);
                    })
                }

                conn.query(sql_insert, function (err, result) {
                    if (err) {
                        return conn.rollback(function (err) {
                            callback(err);
                        })
                    }

                    conn.commit(function (err) {
                        if (err) {
                            return connection.rollback(function () {
                                callback(err);
                            });
                        }
                    });

                    callback(null);
                });
            });
        })
    });
    ////执行事务：先删除旧关系数据，后插入新关系数据
    //var transaction = db.createTransaction();
    //
    //transaction.on('commit', function () {
    //    //callback(null);
    //}).on('rollback', function (err) {
    //    callback(err);
    //});
    //
    //transaction.query(sql_delete).on('result', function (result, fields) {
    //    transaction.query(sql_insert).on('result', function(result){
    //        onsole.log('result err: ', result);
    //        callback(null, result);
    //    });
    //});
};

Hotclass.prototype.findById = function (id, callback) {
    var sql = 'select id, keyword, state, createTime, ' +
        '(select roleId from hotwords_role where hotwordsId=hotwords.id ) as roleId, ' +
        '(select name from role where hotwordsId=hotwords.id ) as roleId, ' +
        'from hotwords where id=' + id;
    db.query(sql, callback);
};

Hotclass.prototype.find = function (hotclassType, keyword, callback) {
    //console.log('Hotclass, keyword, hotclassType: ', keyword, hotclassType);
    var sql;
    if (hotclassType === 1) {
        sql = 'select keyword, hotclassId, hotclassName, score from ' + table + ' where keyword="' + keyword + '"';
    } else if (hotclassType === 2) {
        sql = 'select keyword, hotclassId, hotclassName, score, expireTime, state from ' + table + ' where keyword="' + keyword + '"';
    }
    db.query(sql, callback);
};

Hotclass.prototype.intervention = function (ids, callback) {
    var str_ids = ids.join(',');
    var sql = 'update hotwords set intervention=2 where id in (' + str_ids + ')';
    db.query(sql, callback);
};

Hotclass.prototype.sortnum = function (id, sort_num, callback) {
    var modify_time = dateformat(Date.now(), 'yyyy-mm-dd HH-MM-ss');

    if (sort_num >= 1 && sort_num <= 20) {
        var selectSortnum = 'select id from hotwords where sort_num=' + sort_num;
        db.query(selectSortnum, function (err, rows, fields) {
            if (err) {
                callback(err);
                return;
            }
            //更新目标行
            var updateTargetSornum = 'update hotwords set sort_num=' + sort_num + ' ,modify_time="' + modify_time + '" where id=' + id;
            console.log('updateTargetSornum: ', updateTargetSornum);
            //如果rows的长度不为0，则需要使用事务，更新目标行和替代行
            if (rows.length > 0 && rows[0].id) {
                var updateReplaceSornum = 'update hotwords set sort_num=999' + ' ,modify_time="' + modify_time + '" where id=' + rows[0].id;
                console.log('updateReplaceSornum: ', updateReplaceSornum);
                //执行事务：先删除旧关系数据，后插入新关系数据
                var transaction = db.createTransaction();

                transaction.on('commit', function () {
                    //callback(null);
                }).on('rollback', function (err) {
                    console.log('rollback err: ', err);
                    callback(err);
                });

                transaction.query(updateReplaceSornum).
                    query(updateTargetSornum).on('result', function (result, fields) {
                        console.log('result err: ', result);
                        callback(null, result);
                        //console.log('err: ',err, 'result: ',result, 'fields: ',fields);
                    });

            } else {
                db.query(updateTargetSornum, callback);
            }

            ////执行事务：先删除旧关系数据，后插入新关系数据
            //var transaction = db.createTransaction();
            //transaction.on('commit', function () {
            //    //callback(null);
            //}).on('rollback', function (err) {
            //    callback(err);
            //});
            //
            //transaction.query(updateReplaceSornum).
            //    query(updateTargetSornum).on('result', function (result) {
            //        //if (modules.length > 0) {
            //        //    transaction.query(updateTargetSornum);
            //        ////}
            //        //callback(result);
            //        console.log(result);
            //    }).autoCommit(false);
        });
    }


    //var sql = 'update hotwords set state={{state}}, roleId={{roleId}} where id={{id}}';
    //sql = swig.compile(sql)({state: state, roleId: roleId, id: id});
    //db.query(sql, callback);
};

Hotclass.prototype.delete = function (ids, callback) {
    var ids_str = ids.join(',');
    var sql = 'delete from hotwords where id in (' + ids_str + ')';
    db.query(sql, callback);
};


module.exports = Hotclass;


