/**
 * Created by yishuangxi on 2016/1/11.
 */
var swig = require('swig');
var db = require('../lib/database');

var Role = function (data) {
};

Role.prototype.create = function (name, state, callback) {
    var sql = 'insert into role values (null, "{{name}}", {{state}}, {{createTime}})';
    sql = swig.compile(sql)({
        name: name,
        state: state,
        createTime: parseInt(Date.now() / 1000)
    });
    db.query(sql, callback);
};

Role.prototype.findById = function (id, callback) {
    var sql = 'select id, name, state, createTime from role where id=' + id;
    db.query(sql, callback);
};

Role.prototype.find = function (name, state, callback) {
    var sql = 'select id, name, state, createTime, ' +
        '(select group_concat(modularId) from role_modular where roleId=role.id) as modularList ' +
        'from role where name like "%{{name}}%" ';
    if (state === 1 || state === 0) {
        sql += 'and state={{state}} '
    }
    sql += 'order by createTime desc ';
    sql = swig.compile(sql)({
        name: name,
        state: state
    });
    db.query(sql, callback);
};

Role.prototype.update = function (id, name, state, modularList, callback) {
    var roleId = id;
    //构建更新role表的数据
    var roleUpdate = 'update role set name="{{name}}", state={{state}} where id={{id}}';
    roleUpdate = swig.compile(roleUpdate)({name:name, state:state, id:id});

    //构建删除sql
    var roleModularDelete = 'delete from role_modular where roleId=' + roleId;

    //构建插入sql
    var roleModularInsert = 'insert into role_modular values ';
    var roleModularInsertValues = [];
    for (var i = 0; i < modularList.length; i++) {
        roleModularInsertValues.push('(' + roleId + ',' + modularList[i] + ')');
    }
    roleModularInsert += roleModularInsertValues.join(',');

    //执行事务：先删除旧关系数据，后插入新关系数据
    var transaction = db.createTransaction();
    console.log('roleModularInsert1: ', roleModularInsert);
    transaction.on('commit', function () {
        //callback(null);
    }).on('rollback', function (err) {
        callback(err);
    });

    if(modularList.length > 0){
        transaction.query(roleUpdate).
            query(roleModularDelete).on('result', function (result) {
                transaction.query(roleModularInsert).on('result', function (result) {
                    callback(null, result);
                    //console.log('err: ',err, 'result: ',result, 'fields: ',fields);
                });
            }).autoCommit(false);
    }else{
        transaction.query(roleUpdate).
            query(roleModularDelete).on('result', function (result) {
                callback(null ,result);
                //if (modularList.length > 0) {
                //    console.log('roleModularInsert2: ', roleModularInsert);
                //    transaction.query(roleModularInsert).on('result', function (result) {
                //        callback(null, result);
                //        //console.log('err: ',err, 'result: ',result, 'fields: ',fields);
                //    });
                //}else{
                //
                //}
            });
    }

};

module.exports = Role;

/*
 * Model API HTTP Method Example Path

 create()

 POST

 /locations

 upsert()

 PUT

 /locations

 exists()

 GET

 /locations/:id/exists

 findById()

 GET

 /locations/:id

 find()

 GET

 /locations

 findOne()

 GET

 /locations/findOne

 deleteById()

 DELETE

 /locations/:id

 count()

 GET

 /locations/count

 prototype.updateAttributes()

 PUT

 /locations/:id


 * */

