/**
 * Created by yishuangxi on 2016/1/11.
 */
var swig = require('swig');
var db = require('../lib/database');
var async = require('async');
var dateformat = require('dateformat');
var Hotword = function (data) {
};
/*
 * `id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `keywords` varchar(16) NOT NULL COMMENT '热词',
 `sort_num` int(11) NOT NULL DEFAULT '999' COMMENT '排序，asc，保留字段',
 `intervention` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1,人工干预；0，系统生成。原表里面没有屏蔽词选项，这里添加一个屏蔽词，值为2',
 `org_code` int(10) NOT NULL COMMENT '商家ID',
 `store_id` int(10) NOT NULL COMMENT '门店ID',
 `create_time` datetime NOT NULL,
 `modify_time` datetime DEFAULT NULL COMMENT "修改时间，默认为null",
 `effective_time` datetime DEFAULT NULL COMMENT "生效时间， 默认为null",
 `expire_time` datetime DEFAULT NULL COMMENT "过期时间， 默认为null"
 ) ENGINE=InnoDB AUTO_INCREMENT=1873084 DEFAULT CHARSET=utf8 COMMENT='热词信息表';
 * */
Hotword.prototype.create = function (keywords, store_id, intervention, effective_time, expire_time, callback) {
    var sort_num = 999;
    var org_code = 1111;
    var create_time = '2015-01-01 12:12:12';
    var sql = 'insert into hotwords values (null, "{{keywords}}", {{sort_num}}, {{intervention}}, {{org_code}},' +
        ' {{store_id}}, "{{create_time}}", "{{modify_time}}", "{{effective_time}}", "{{expire_time}}")';
    sql = swig.compile(sql)({
        keywords: keywords,
        sort_num: sort_num,
        intervention: intervention,
        org_code: org_code,
        store_id: store_id,
        create_time: create_time,
        modify_time: create_time,
        effective_time: effective_time,
        expire_time: expire_time
    });
    db.query(sql, callback);
};

Hotword.prototype.findById = function (id, callback) {
    var sql = 'select id, keywords, state, createTime, ' +
        '(select roleId from hotwords_role where hotwordsId=hotwords.id ) as roleId, ' +
        '(select name from role where hotwordsId=hotwords.id ) as roleId, ' +
        'from hotwords where id=' + id;
    db.query(sql, callback);
};

Hotword.prototype.find = function (intervention, keywords, org_code, store_id, page, callback) {
    var pageNum = 30;
    var conLimit = 'limit ' + pageNum + ' offset ' + parseInt(page - 1) * pageNum;
    var conWhere = 'where keywords like "%{{keywords}}%" ';
    if (org_code) {
        conWhere += 'and  org_code={{org_code}} ';
    }
    if (store_id) {
        conWhere += 'and  store_id={{store_id}} ';
    }
    if (intervention === 0 || intervention === 1 || intervention === 2) {
        conWhere += 'and intervention={{intervention}} '
    }
    conWhere = swig.compile(conWhere)({
        keywords: keywords,
        org_code: org_code,
        intervention: intervention,
        store_id: store_id
    });
    var conPageSelect = 'select id, keywords, sort_num, intervention, org_code, store_id,' +
        'DATE_FORMAT(create_time, "%Y-%m-%d %H:%i:%S") as create_time, DATE_FORMAT(modify_time, "%Y-%m-%d %H:%i:%S") as modify_time,' +
        'DATE_FORMAT(effective_time, "%Y-%m-%d") as effective_time,  DATE_FORMAT(expire_time, "%Y-%m-%d") as expire_time ';
    var conFrom = 'from hotwords ';
    var conCountSelect = 'select count(id) as count ';

    var sqlPage = conPageSelect + conFrom + conWhere + ' order by id desc ' + conLimit;
    var sqlCount = conCountSelect + conFrom + conWhere;
    async.parallel({
        count: function (cb) {
            db.query(sqlCount, function (err, rows, fields) {
                cb(err, rows);
            });
        },
        rows: function (cb) {
            db.query(sqlPage, function (err, rows, fields) {
                cb(err, rows);
            });
        }
    }, function (err, results) {
        callback(err, results.count, results.rows);
    });
};
Hotword.prototype.intervention = function (ids, callback) {
    var str_ids = ids.join(',');
    var sql = 'update hotwords set intervention=2 where id in (' + str_ids + ')';
    db.query(sql, callback);
};

Hotword.prototype.sortnum = function (id, sort_num, callback) {
    var modify_time = dateformat(Date.now(), 'yyyy-mm-dd HH-MM-ss');

    var selectSortnum = 'select id from hotwords where sort_num=' + sort_num;
    db.query(selectSortnum, function (err, rows, fields) {
        if (err) {
            callback(err);
            return;
        }
        //更新目标行
        var updateTargetSornum = 'update hotwords set sort_num=' + sort_num + ' ,modify_time="' + modify_time + '" where id=' + id;
        console.log('updateTargetSornum: ', updateTargetSornum);
        //如果rows的长度不为0，则需要使用事务，更新目标行和替代行
        if (rows.length > 0 && rows[0].id) {
            var updateReplaceSornum = 'update hotwords set sort_num=999' + ' ,modify_time="' + modify_time + '" where id=' + rows[0].id;
            console.log('updateReplaceSornum: ', updateReplaceSornum);
            //执行事务：先删除旧关系数据，后插入新关系数据
            var transaction = db.createTransaction();

            transaction.on('commit', function () {
                //callback(null);
            }).on('rollback', function (err) {
                console.log('rollback err: ', err);
                callback(err);
            });

            transaction.query(updateReplaceSornum).
                query(updateTargetSornum).on('result', function (result, fields) {
                    console.log('result err: ', result);
                    callback(null, result);
                    //console.log('err: ',err, 'result: ',result, 'fields: ',fields);
                });

        } else {
            db.query(updateTargetSornum, callback);
        }
    });


    //var sql = 'update hotwords set state={{state}}, roleId={{roleId}} where id={{id}}';
    //sql = swig.compile(sql)({state: state, roleId: roleId, id: id});
    //db.query(sql, callback);
};

Hotword.prototype.delete = function (ids, callback) {
    var ids_str = ids.join(',');
    var sql = 'delete from hotwords where id in (' + ids_str + ')';
    db.query(sql, callback);
};


module.exports = Hotword;


