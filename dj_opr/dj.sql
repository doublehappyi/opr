CREATE DATABASE IF NOT EXISTS dj_opr DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use dj_opr;

#角色表
CREATE TABLE IF NOT EXISTS `role`(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(16) UNIQUE NOT NULL,
  `state` INT(1) DEFAULT 0,
  `createTime` INT(10) NOT NULL
);
INSERT INTO `role` VALUES (1, '管理员', 1, 1451635482);
INSERT INTO `role` VALUES (2, '搜索组', 1, 1451635482);
INSERT INTO `role` VALUES (3, '游客', 1, 1451635482);

#用户表
CREATE TABLE IF NOT EXISTS `user`(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username` VARCHAR(16) UNIQUE NOT NULL,
  `password` VARCHAR(16) NOT NULL,
  `state` INT(1) DEFAULT 0,
  `createTime` INT(10) NOT NULL,
  `roleId` INT(10) NOT NULL
);
INSERT INTO user VALUES (1, 'admin', 'admin', 1, 1451635482, 1);
INSERT INTO user VALUES (2, 'search', '88888888', 1, 1451635482, 2);
INSERT INTO user VALUES (3, 'guest', '88888888', 1, 1451635482, 3);


#功能模块表
CREATE TABLE IF NOT EXISTS `modular`(
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(16) UNIQUE NOT NULL,
  `title` VARCHAR(128) UNIQUE NOT NULL,
  `url` VARCHAR(255) UNIQUE NOT NULL,
  `file` VARCHAR(64) DEFAULT NULL,
  `script` VARCHAR(64) DEFAULT NULL,
  `state` INT(1) DEFAULT 0,
  `createTime` INT(10) NOT NULL
);
insert into modular values(1, 'user', '用户管理', '/user', 'file1', 'script1', 1, 1451635486);
insert into modular values(2, 'role', '角色管理', '/role', 'file2', 'script2', 1, 1451635486);
insert into modular values(3, 'modular', '模块管理', '/modular', 'file3', 'script3', 1, 1451635486);
insert into modular values(4, 'hotword', '热词管理', '/hotword', 'file4', 'script4', 1, 1451695486);

#角色模块关联表
CREATE TABLE IF NOT EXISTS `role_modular`(
  `roleId` INT(10) NOT NULL,
  `modularId` INT(10) NOT NULL,
  PRIMARY KEY (`roleId`, `modularId`)
);
insert into `role_modular` values(1, 1);
insert into `role_modular` values(1, 2);
insert into `role_modular` values(1, 3);
insert into `role_modular` values(1, 4);
insert into `role_modular` values(2, 4);


CREATE TABLE `hotwords` (
`id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
`keywords` varchar(16) NOT NULL COMMENT '热词',
`sort_num` int(11) NOT NULL DEFAULT '999' COMMENT '排序,asc,保留字段',
`intervention` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1,人工干预；0,系统生成。原表里面没有屏蔽词选项,这里添加一个屏蔽词,值为2',
`org_code` int(10) NOT NULL COMMENT '商家ID',
`store_id` int(10) NOT NULL COMMENT '门店ID',
`create_time` datetime NOT NULL,
`modify_time` datetime DEFAULT NULL COMMENT "修改时间,默认为null",
`effective_time` date DEFAULT NULL COMMENT "生效时间, 默认为null",
`expire_time` date DEFAULT NULL COMMENT "过期时间, 默认为null"
) ENGINE=InnoDB AUTO_INCREMENT=1873084 DEFAULT CHARSET=utf8 COMMENT='热词信息表';

insert into `hotwords` values
(null, '苹果', 999, 0, 10000, 10001, "2016-01-01 12:12:00", "2016-01-01 12:12:00", "2016-02-01", "2017-01-01"),
(null, '牛奶', 1, 1, 10000, 10001, "2016-01-01 12:12:00", "2016-01-01 12:12:00", "2016-02-01", "2017-01-01"),
(null, '香蕉', 10, 2, 10000, 10001, "2016-01-01 12:12:00", "2016-01-01 12:12:00", "2016-02-01", "2017-01-01");


#hotclassOriginal：根据数据文件创建的原始热门类目数据表
create TABLE `hotclass_origin`(
  `keyword` VARCHAR(32) NOT NULL,
  `hotclassId` INT(10) NOT NULL,
  `hotclassName` VARCHAR(64) NOT NULL,
  `score` INT(10) NOT NULL,
  PRIMARY KEY(`keyword`, `hotclassId`)
);
INSERT INTO `hotclass_origin` VALUES ("牛奶", 1, "乳制品", 100), ("牛奶", 2, "零食", 200), ("牛奶", 3, "蛋糕", 300);
INSERT INTO `hotclass_origin` VALUES ("水果", 4, "热带水果", 100), ("水果", 5, "冬季水果", 200), ("水果", 6, "夏季水果", 300);
INSERT INTO `hotclass_origin` VALUES ("衣服", 7, "冬衣服", 100), ("衣服",8, "夏衣服", 200), ("衣服",9 , "秋衣服", 300);

--#热门类目关键词表
--CREATE TABLE `hotclass_keyword` (
--  `keyword` VARCHAR(32) NOT NULL,
--  `expireTime` datetime NOT NULL,
--  `state` INT(1) DEFAULT 0 COMMENT "0：未发布,1:已发布",
--  PRIMARY KEY (`keyword`)
--);
--INSERT INTO `hotclass_keyword` VALUES ("牛奶", "2015-01-01 12:12:12", 0), ("水果", "2015-01-01 12:12:12", 1), ("衣服", "2015-01-01 12:12:12", 1);

#热门类目和关键词关系表
create TABLE `hotclass`(
  `keyword` VARCHAR(32) NOT NULL,
  `hotclassId` INT(10) NOT NULL,
  `hotclassName` VARCHAR(64) DEFAULT NULL,
  `score` INT(10) NOT NULL,
  `expireTime` datetime NOT NULL,
  `state` INT(1) DEFAULT 0 COMMENT "0：未发布,1:已发布",
  PRIMARY KEY(`keyword`, `hotclassId`)
);
INSERT INTO `hotclass` VALUES ("牛奶", 1, "乳制品", 1001 ,"2015-01-01 12:12:12", 1), ("牛奶", 2, "零食", 2001 ,"2015-01-01 12:12:12", 1), ("牛奶", 3, "蛋糕", 3001 ,"2015-01-01 12:12:12", 1);
INSERT INTO `hotclass` VALUES ("水果", 4, "热带水果", 1001 ,"2015-01-01 12:12:12", 0), ("水果", 5, "冬季水果", 2001 ,"2015-01-01 12:12:12", 0);
INSERT INTO `hotclass` VALUES ("衣服", 7, "冬衣服", 1001 ,"2015-01-01 12:12:12", 1), ("衣服", 9 , "秋衣服", 3001 ,"2015-01-01 12:12:12", 1);


