/**
 * Created by yishuangxi on 2016/1/11.
 */
var router = require('express').Router();
var UserModel = require('../models/UserModel');
var user = new UserModel();
var auth = require('../middlewares/auth');
router.get('/', function (req, res, next) {
    res.render('user/index.html');
});

router.get('/lists.json', function (req, res, next) {
    var username = req.query.username,
        roleId = req.query.roleId,
        state = req.query.state,
        page = req.query.page;
    //console.log(username, roleId, state, page);
    user.find(username, roleId, state, page, function (err, count, rows) {
        if (err) {
            res.json(err);
        }
        res.json({code: 1, count: count[0].count, rows: rows});
    });
});

router.post('/', function(req, res, next){
    var username = req.body.username,
        roleId = req.body.roleId,
        state = req.body.state;

    user.create(username, roleId, state, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/', function(req, res, next){
    var id = req.body.id,
        roleId = req.body.roleId,
        state = req.body.state;

    user.update(id, roleId, state, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/password', function(req, res, next){
    var id = req.body.id;

    user.password(id, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

module.exports = router;