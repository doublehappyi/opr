/**
 * Created by yishuangxi on 2016/1/11.
 */
var router = require('express').Router();
var HotwordModel = require('../models/HotwordModel');
var hotword = new HotwordModel();
router.get('/', function (req, res, next) {
    res.render('hotword/index.html');
});

router.get('/lists.json', function (req, res, next) {
    var intervention = parseInt(req.query.intervention),
        keywords = req.query.keywords,
        org_code = req.query.org_code,
        store_id = parseInt(req.query.store_id),
        page = parseInt(req.query.page);
    hotword.find(intervention, keywords, org_code, store_id, page, function (err, count, rows) {
        if (err) {
            res.json(err);
        }
        res.json({code: 1, count: count[0].count,page:page, rows: rows});
    });
});

router.post('/', function(req, res, next){
    var keywords = req.body.keywords,
        store_id = parseInt(req.body.store_id),
        intervention = parseInt(req.body.intervention),
        effective_time = req.body.effective_time,
        expire_time = req.body.expire_time;

    hotword.create(keywords, store_id, intervention, effective_time, expire_time, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/intervention', function(req, res, next){
    var ids = JSON.parse(req.body.ids);
    if(ids.length === 0){
        res.json({code:0, msg:'ids格式不对'});
    }
    hotword.intervention(ids, function(err, rows){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/sortnum', function(req, res, next){
    var id = parseInt(req.body.id),
        sort_num = parseInt(req.body.sort_num);

    hotword.sortnum(id, sort_num, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});


router.delete('/', function(req, res, next){
    var ids = JSON.parse(req.body.ids);
    console.log('ids: ',ids);
    if(ids.length === 0){
        res.json({code:0, msg:'ids格式不对'});
    }
    hotword.delete(ids, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

module.exports = router;