/**
 * Created by yishuangxi on 2016/1/11.
 */
var router = require('express').Router();
var HotclassModel = require('../models/HotclassModel');
var hotclass = new HotclassModel();
router.get('/', function (req, res, next) {
    res.render('hotclass/index.html');
});

router.get('/lists.json', function (req, res, next) {
    var keyword = decodeURIComponent(req.query.keyword),
        hotclassType = parseInt(req.query.hotclassType);
    if(hotclassType === 1 || hotclassType === 2){
        console.log('keyword, hotclassType: ', keyword, hotclassType);
        hotclass.find(hotclassType, keyword, function (err, rows, fields) {
            if (err) {
                res.json(err);
            }
            res.json({code: 1, rows: rows});
        });
    }else{
        res.json({code:0, msg:"hotclassType参数错误"});
    }

});

router.post('/save', function(req, res, next){
    var keyword = req.body.keyword,
        rows = JSON.parse(req.body.rows),
        expireTime = req.body.expire_time,
        state = parseInt(req.body.state);

    hotclass.save(keyword, rows, expireTime, state, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/publish', function(req, res, next){
    var ids = JSON.parse(req.body.ids);
    if(ids.length === 0){
        res.json({code:0, msg:'ids格式不对'});
    }
    hotclass.intervention(ids, function(err, rows){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/sortnum', function(req, res, next){
    var id = parseInt(req.body.id),
        sort_num = parseInt(req.body.sort_num);

    hotclass.sortnum(id, sort_num, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});


router.delete('/', function(req, res, next){
    var ids = JSON.parse(req.body.ids);
    console.log('ids: ',ids);
    if(ids.length === 0){
        res.json({code:0, msg:'ids格式不对'});
    }
    hotclass.delete(ids, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

module.exports = router;