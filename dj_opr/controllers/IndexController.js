/**
 * Created by yishuangxi on 2016/1/11.
 */
var router = require('express').Router();
var passport = require('passport');
var auth = require('../middlewares/auth');
var IndexModel = require('../models/IndexModel');
var index = new IndexModel();
router.get('/', function (req, res, next) {
    var roleId = req.user.roleId;
    index.find(roleId, function(err, rows, fields){
        if(err){
            res.send(err);
        }else{

            var m = {};
            if(rows && rows.length){
                for (var i = 0; i < rows.length; i++){
                    var row = rows[i];
                    for(var k in row){
                        if(row.hasOwnProperty(k)){
                            m[row[k]] = true;
                        }
                    }
                }
            }
            //res.json(m);
            console.log('mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm: ', m);
            res.render('index.html', {user:req.user, m:m});
        }
    });
    //res.render('index.html', {user:req.user});
});

router.get('/welcome', function(req,res, next){
    res.render('welcome.html');
});

router.post('/login',
    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login'
    }));

router.get('/logout', function(req, res, next){
    req.logout();
    res.redirect('/login');
});

router.get('/login', function(req, res, next){
    console.log('login ...');
    res.render('login.html');
});

module.exports = router;