/**
 * Created by yishuangxi on 2016/1/11.
 */
var router = require('express').Router();
var ModularModel = require('../models/ModularModel');
var modular = new ModularModel();
router.get('/', function (req, res, next) {
    res.render('modular/index.html');
});

router.get('/lists.json', function (req, res, next) {
    var name = req.query.name,
        title = req.query.title,
        state = parseInt(req.query.state);
    modular.find(name, title, state, function (err, rows) {
        if (err) {
            res.json(err);
        }
        res.json({code: 1, count: rows && rows.length, rows: rows});
    });
});

router.post('/', function(req, res, next){
    var name = req.body.name,
        title = req.body.title,
        url = req.body.url,
        file = req.body.file,
        script = req.body.script,
        state = parseInt(req.body.state);

    modular.create(name, title, url, file, script, state, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/', function(req, res, next){
    var id = req.body.id,
        title = req.body.title,
        url = req.body.url,
        file = req.body.file,
        script = req.body.script,
        state = parseInt(req.body.state);

    modular.update(id, title, url, file, script, state, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/password', function(req, res, next){
    var id = req.body.id;

    modular.password(id, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

module.exports = router;