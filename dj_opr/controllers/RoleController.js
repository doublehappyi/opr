/**
 * Created by yishuangxi on 2016/1/11.
 */
var router = require('express').Router();
var RoleModel = require('../models/RoleModel');
var role = new RoleModel();

router.get('/', function(req, res, next){
    res.render('role/index.html');
});

router.get('/lists.json', function(req, res, next){
    var name = req.query.name,
        state = parseInt(req.query.state);
    console.log('name, state: ',name, state);
    role.find(name, state, function (err, rows) {
        if (err) {
            res.json(err);
        }
        res.json({code: 1, count: rows && rows.length, rows: rows});
    });
        
});

router.post('/', function(req, res, next){
    var name = req.body.name,
        state = req.body.state;

    role.create(name, state, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

router.put('/', function(req, res, next){
    var id = parseInt(req.body.id),
        name = req.body.name,
        state = parseInt(req.body.state),
        modularList = JSON.parse(req.body.modularList||'[]');
    //console.log('id, name, state, modularList: ', id, name, state, modularList);
    role.update(id, name, state, modularList, function(err, rows, fields){
        if (err) {
            res.json(err);
        }
        res.json({code: 1, msg:'ok', rows:rows});
    });
});

module.exports = router;