/**
 * Created by db on 16/1/10.
 */
var db = require('../lib/database');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    console.log("deserializeUser: ", user);
    done(null, user);
});

var mysql = require('mysql');
var pool = require('../lib/database').pool;
var localStrategy = new LocalStrategy(function(username, password, done) {
    process.nextTick(function() {
        pool.getConnection(function (err, conn) {
            if (err) {
                return;
            }
            //必须用户名密码匹配且用户状态为1：即激活状态
            var sql_str = 'select id, username, roleId, (select name from role where id=user.id ) as roleName from user where username=? and password=? and state=1';
            sql_str = mysql.format(sql_str, [username, password]);
            conn.query(sql_str, function (err, rows, fields) {
                if (err) {
                    return;
                }
                done(null, rows[0]);
                conn.release();
            });
        });
    });
});

function isAuthorized(req, res, next){
    var exceptUrlList = ['/login', '/logout', '/profile', '/', '/welcome'];
    var originalUrl = req.originalUrl;
    if(exceptUrlList.indexOf(originalUrl) >=0){
        next();
    }else{
        var roleId = parseInt(req.user.roleId),
            username = req.user.username,
            modularName = originalUrl.split('/')[1];

        if(roleId === 1 || username === 'admin'){//如果用户是admin用户，其roleId为1
            next();
        }else{
            //角色模块关联表，且模块是激活状态，即state=1
            var sql = 'select roleId from role_modular where roleId='+roleId + ' and modularId=(select id from modular where name="'+modularName+'")';
            db.query(sql, function(err, rows){
                if(err){res.send(err);return;}
                //console.log('rows: ', rows);
                if(rows.length === 0){
                    res.send('您还没有开通权限!');
                    return;
                }
                next();
            });
        }
    }

}

module.exports = {
    isAuthenticated:function(req, res, next){
        var exceptUrlList = ['/login', '/welcome'];
        var originalUrl = req.originalUrl;
        //添加不需要授权的例外页面
        if(exceptUrlList.indexOf(originalUrl)>=0){
            console.log('originalUrl: ', originalUrl);
            next();
        } else{
            if(req.isAuthenticated()){
                next()
            }else{
                res.redirect('/login');
            }
        }
    },
    localStrategy:localStrategy,
    isAuthorized:isAuthorized
}