/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function(){
    //定义一些全局变量
    var intervention='', keyword='', store_id='', org_code='', page = 1;
    //查询
    query();

    keyword_add();
    keyword_edit();
    keyword_delete();
    keyword_choose();

    save();

    function save(){

    }

    function keyword_choose(){
        $(document).on('click', '[data-action=action-choose]', function(){
            var $this = $(this);
            var $tr = $this.closest('tr');
            console.log(this.checked);
            if(this.checked){
                $this.attr('disabled', 'disabled')
                var data = {
                    hotclassId:$tr.find('[data-hotclassId]').attr('data-hotclassId'),
                    hotclassName:$tr.find('[data-hotclassName]').attr('data-hotclassName'),
                    score:$tr.find('[data-score]').attr('data-score')
                }
                insert(data);
            }
        });

        $(document).on('blur', '.input-dblclick', function(){
            var $this = $(this);
            var val = $this.val();
            $this.hide();
            $this.prev('.label-dblclick').show().text(val);
        });
    }


    function keyword_delete(){
        var $hotclassAuto = $('#hotclass-auto');
        $(document).on('click', '[data-action=action-delete]', function(){
            var $this = $(this);
            var $tr = $this.closest('tr');
            $tr.remove();
        });

        $(document).on('blur', '.input-dblclick', function(){
            var $this = $(this);
            var val = $this.val();
            $this.hide();
            $this.prev('.label-dblclick').show().text(val);
        });
    }



    function keyword_edit(){
        $(document).on('dblclick', '.box-dblclick', function(){
            var $this = $(this);
            var $label = $this.find('.label-dblclick');
            var $input = $this.find('.input-dblclick');
            var val = $label.text();
            $label.hide();
            $input.show().focus().val(val);
        });

        $(document).on('blur', '.input-dblclick', function(){
            var $this = $(this);
            var val = $this.val();
            $this.hide();
            $this.prev('.label-dblclick').show().text(val);
        });
    }

    function keyword_add(data){
        var $keyword = $('.keyword');
        $('[data-action=action-add]').click(function(){
            var keyword = $keyword.eq(1).text();
            if(!keyword){
                alert("您还没有查询关键词");
                return;
            }
            insert(data);
        });
    }

    function insert(data){
        var defaults = {
            keyword:keyword,
            hotclassId:'',
            hotclassName:'自动填充，无需填写',
            score:''
        };
        var $tbody = $('#hotclass-rg').find('.box-tbody');
        var data = $.extend({}, defaults, data);
        $tbody.prepend(Mustache.render($('#tpl-rg-tr').html(), data));
    }

    function query(){
        var $query = $('#form-query').find('[name=keyword]');
        var $keyword = $('.keyword');
        $('#form-query-btn').click(function(){
            keyword = $query.val();
            $keyword.text(keyword);
            table_auto();
            table_rg();
        });
    }

    function table_auto(){
        var hotclassType = 1;
        var $hotclass = $('#hotclass-auto');
        var url = '/hotclass/lists.json?keyword='+encodeURIComponent(keyword) + '&hotclassType='+hotclassType;
        var $loading = $hotclass.find('.loading');
        var $tbody = $hotclass.find('.box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.code === 1) {
                //for (var i = 0; i < data.rows.length; i++) {
                //    if (data.rows[i]['createTime']) data.rows[i]['createTime'] = Yi.formatTime(data.rows[i]['createTime']);
                //}
                $tbody.append(Mustache.render($('#tpl-auto').html(), data));
            }
        }).always(function(){
            $loading.hide();
        });
    }

    function table_rg(){
        var hotclassType = 2;
        var $hotclass = $('#hotclass-rg');
        var url = '/hotclass/lists.json?keyword='+encodeURIComponent(keyword) + '&hotclassType='+hotclassType;
        var $loading = $hotclass.find('.loading');
        var $tbody = $hotclass.find('.box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.code === 1) {
                $tbody.append(Mustache.render($('#tpl-rg').html(), data));
            }
        }).always(function(){
            $loading.hide();
        });
    }
});


