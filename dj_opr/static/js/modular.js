/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function(){
    //定义一些全局变量
    var name='', title='', state='';
    //表格
    table();
    //查询
    query();
    //新增
    add();
    //弹窗编辑
    edit();
    //状态，编辑，重置密码
    //action();

    action_state();
    action_edit();

    function action_state(){
        $(document).on('click', '[data-action=change-state]', function () {
            var $this = $(this);
            var $tr = $this.closest('tr');
            var $state = $tr.find('[data-state]');
            var id = $tr.find('[data-id]').attr('data-id');
            var name = $tr.find('[data-name]').attr('data-name');
            var title = $tr.find('[data-title]').attr('data-title');
            var url = $tr.find('[data-url]').attr('data-url');
            var file = $tr.find('[data-file]').attr('data-file');
            var script = $tr.find('[data-script]').attr('data-script');
            var state = parseInt( $state.attr('data-state')) == 0 ? 1:0;

            $this.attr('disabled', 'disabled');
            $.ajax('/modular',{
                method:'PUT',
                data:{
                    id:id,
                    name:name,
                    title:title,
                    url:url,
                    file:file,
                    script:script,
                    state:state
                }
            }).done(function(data){
                if(data && data.code == 1){
                    $state.attr('data-state', state);
                    if(state){
                        $state.find('label').removeClass('label-default').addClass('label-success').text('已启用');
                        $this.text('禁用');
                    }else{
                        $state.find('label').removeClass('label-success').addClass('label-default').text('已禁用');
                        $this.text('启用');
                    }
                }
            }).always(function(data){
                $this.attr('disabled', false);
            });
        });
    }

    function action_edit(){
        $(document).on('click', '[data-action=modular-edit]', function () {
            var $this = $(this);
            var $tr = $this.closest('tr');
            var $modalEdit = $('#modal-edit');
            var $id = $modalEdit.find('[name=id]');
            var $name = $modalEdit.find('[name=name]');
            var $title = $modalEdit.find('[name=title]');
            var $url = $modalEdit.find('[name=url]');
            var $file = $modalEdit.find('[name=file]');
            var $script = $modalEdit.find('[name=script]');
            var $state = $modalEdit.find('[name=state]');

            $id.val($tr.find('[data-id]').attr('data-id'));
            $name.val($tr.find('[data-name]').attr('data-name'));
            $title.val($tr.find('[data-title]').attr('data-title'));
            $url.val($tr.find('[data-url]').attr('data-url'));
            $file.val($tr.find('[data-file]').attr('data-file'));
            $script.val($tr.find('[data-script]').attr('data-script'));
            $state.val($tr.find('[data-state]').attr('data-state'));

            $('#modal-edit').modal('show');
        });
    }


    function query(){
        var $form = $('#form-query');
        $('#form-query-btn').click(function(){
            name = $form.find('[name=name]').val();
            title = $form.find('[name=title]').val();
            state = $form.find('[name=state]').val();
            table();
        });
    }

    function add(){
        var $modalAdd = $('#modal-add');
        var $submit = $modalAdd.find('[data-action=modal-submit]');
        var $name = $modalAdd.find('[name=name]');
        var $title = $modalAdd.find('[name=title]');
        var $url = $modalAdd.find('[name=url]');
        var $file = $modalAdd.find('[name=file]');
        var $script = $modalAdd.find('[name=script]');
        var $state = $modalAdd.find('[name=state]');
        $submit.click(function(){
            var name = $name.val(), state = $state.val(), title=$title.val(), url=$url.val(), file=$file.val(),script=$script.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/modular', {
                method:'POST',
                data:{
                    name:name,
                    title:title,
                    url:url,
                    file:file,
                    script:script,
                    state:state
                }
            }).done(function(data){
                if(data.code === 1){
                    $modalAdd.modal('hide');
                    table();
                }else{
                    alert("添加失败: "+data.msg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function edit(){
        var $modalEdit = $('#modal-edit');
        var $submit = $modalEdit.find('[data-action=modal-submit]');
        var $id = $modalEdit.find('[name=id]');
        var $name = $modalEdit.find('[name=name]');
        var $title = $modalEdit.find('[name=title]');
        var $url = $modalEdit.find('[name=url]');
        var $file = $modalEdit.find('[name=file]');
        var $script = $modalEdit.find('[name=script]');
        var $state = $modalEdit.find('[name=state]');

        $submit.click(function(){
            var id = $id.val(), name=$name.val(), title = $title.val(), url = $url.val(),
                file=$file.val(), script=$script.val(), state = $state.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/modular', {
                method:'PUT',
                data:{
                    id:id,
                    title:title,
                    url:url,
                    file:file,
                    script:script,
                    state:state
                }
            }).done(function(data){
                if(data.code === 1){
                    $modalEdit.modal('hide');
                    table();
                }else{
                    alert("编辑失败: "+data.msg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function table(){
        var url = '/modular/lists.json?name='+name+"&title="+title+"&state="+state;
        var $loading = $('#loading');
        var $tbody = $('#box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.code === 1) {
                for (var i = 0; i < data.rows.length; i++) {
                    if (data.rows[i]['createTime']) data.rows[i]['createTime'] = Yi.formatTime(data.rows[i]['createTime']);
                }
                $tbody.append(Mustache.render($('#tpl-table').html(), data));
            }
        }).always(function(){
            $loading.hide();
        });
    }
});


