/**
 * Created by Yishuangxi on 2016/1/5.
 */

var Yi = {};

Yi.formatTime = function (seconds) {
    var d = new Date(seconds * 1000);
    var year = d.getFullYear();
    var month = d.getMonth() + 1,
        month = month < 10 ? ('0' + month) : month;
    var date = d.getDate(),
        date = date < 10 ? ('0' + date) : date;
    var hour = d.getHours(),
        hour = hour < 10 ? ('0' + hour) : hour;
    var minutes = d.getMinutes(),
        minutes = minutes < 10 ? ('0' + minutes) : minutes;
    var seconds = d.getSeconds(),
        seconds = seconds < 10 ? ('0' + seconds) : seconds;

    return year + '-' + month + '-' + date + ' ' + hour + ':' + minutes + ':' + seconds;
};

Yi.getRandomIntInclusive = function (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

Yi.getQuery = function (key) {
    var queryObj = [], tempArr,
        queryArr = location.search.replace('?', '').split('&');
    for (var i = 0; i < queryArr.length; i++) {
        tempArr = queryArr[i].split('=');
        queryObj[tempArr[0]] = tempArr[1]
    }
    return queryObj[key] ? decodeURIComponent(queryObj[key]) : '';
}