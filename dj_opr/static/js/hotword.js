/**
 * Created by yishuangxi on 2016/1/11.
 */
$(function(){
    //定义一些全局变量
    var intervention='', keywords='', store_id='', org_code='', page = 1;
    //表格
    table();
    //查询
    query();
    //新增
    add();
    //弹窗编辑
    edit();
    sortNumEdit();
    toolbar();
    function toolbar(){
        //改为屏蔽热词
        $('[data-action=change-intervention-2]').click(function(){
            var $checkbox = $('.keywords-checkbox');
            var ids = [];
            for(var i = 0; i < $checkbox.length; i++){
                if($checkbox[i].checked){
                    ids.push(parseInt($checkbox.eq(i).attr('data-id')));
                }
            }
            if(ids.length === 0){
                alert('请选择热词');
                return;
            }
            $.ajax('/hotword/intervention', {
                method:'PUT',
                data:{
                    ids:JSON.stringify(ids)
                }
            }).done(function(data){
                if(data.code === 1){
                    alert('修改成功');
                    table();
                }else{
                    alert('修改失败');
                }
            }).fail(function(err){
                alert("修改失败"+err);
            });
        });


        $('[data-action=delete-words]').click(function(){
            var $checkbox = $('.keywords-checkbox');
            var ids = [];
            for(var i = 0; i < $checkbox.length; i++){
                if($checkbox[i].checked){
                    ids.push(parseInt($checkbox.eq(i).attr('data-id')));
                }
            }
            if(ids.length === 0){
                alert('请选择热词');
                return;
            }
            $.ajax('/hotword', {
                method:'DELETE',
                data:{
                    ids:JSON.stringify(ids)
                }
            }).done(function(data){
                if(data.code === 1){
                    alert('删除成功');
                    table();
                }else{
                    alert('删除失败');
                }
            }).fail(function(err){
                alert("删除失败"+err);
            });

        });
    }

    function sortNumEdit(){
        $(document).on( 'dblclick', '[data-sort_num]', function(e){
            var $this = $(this);
            var $sortNum = $this.find('.sort_num');
            var $sortText = $this.find('.sort_text');

            $sortNum.hide();
            $sortText.show().focus();
        });

        $(document).on( 'blur', '[data-sort_num]', function(e){
            var $this = $(this);
            var $td = $this.closest('td');
            var $sortNum = $td.find('.sort_num');
            var $sortText = $td.find('.sort_text');
            $sortText.attr('disabled', true);
            $.ajax('/hotword/sortnum',{
                method:'PUT',
                data:{
                    sort_num:$sortText.val(),
                    id:$td.attr('data-id')
                }
            }).done(function(data){
                if(data && data.code === 1){
                    //$sortText.hide();
                    //$sortNum.text($sortText.val()).show();
                    table();
                }else{
                    alert('更新失败'+data.msg);
                }
            }).fail(function(err){
                alert('更新失败，网络错误');
            }).always(function(){
                $sortText.attr('disabled', false);
            });
        });
    }




    function query(){
        var $form = $('#form-query');
        $('#form-query-btn').click(function(){
            intervention = $form.find('[name=intervention]').val();
            keywords = $form.find('[name=keywords]').val();
            org_code = $form.find('[name=org_code]').val();
            store_id = $form.find('[name=store_id]').val();
            page = 1;
            table();
        });
    }

    function add(){
        var $modalAdd = $('#modal-add');
        var $submit = $modalAdd.find('[data-action=modal-submit]');
        var $keywords = $modalAdd.find('[name=keywords]');
        var $store_id = $modalAdd.find('[name=store_id]');
        var $intervention = $modalAdd.find('[name=intervention]');
        var $effective_time = $modalAdd.find('[name=effective_time]');
        var $expire_time = $modalAdd.find('[name=expire_time]');
        $submit.click(function(){
            var keywords = $keywords.val(), store_id = $store_id.val(), intervention = $intervention.val(),
                effective_time = $effective_time.val(), expire_time=$expire_time.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/hotword', {
                method:'POST',
                data:{
                    keywords:keywords,
                    store_id:store_id,
                    intervention:intervention,
                    effective_time:effective_time,
                    expire_time:expire_time
                }
            }).done(function(data){
                if(data.code === 1){
                    $modalAdd.modal('hide');
                    table();
                }else{
                    alert("添加失败: "+data.msg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function edit(){
        var $modalEdit = $('#modal-edit');
        var $submit = $modalEdit.find('[data-action=modal-submit]');
        var $id = $modalEdit.find('[name=id]');
        var $store_id = $modalEdit.find('[name=store_id]');
        var $org_code = $modalEdit.find('[name=org_code]');

        $submit.click(function(){
            var id = $id.val(), store_id = $store_id.val(), org_code = $org_code.val();
            $submit.attr('disabled', 'disabled');
            $.ajax('/hotword', {
                method:'PUT',
                data:{
                    id:id,
                    store_id:store_id,
                    org_code:org_code
                }
            }).done(function(data){
                if(data.code === 1){
                    $modalEdit.modal('hide');
                    table();
                }else{
                    alert("编辑失败: "+data.msg);
                }
            }).always(function(){
                $submit.attr('disabled', false);
            });
        });
    }

    function table(){
        var url = '/hotword/lists.json?intervention='+intervention+'&keywords='+keywords+'&store_id='+store_id+"&org_code="+org_code+"&page="+page;
        var $loading = $('#loading');
        var $tbody = $('#box-tbody');
        $tbody.empty();
        $loading.show();
        $.ajax(url, {
            method: 'GET'
        }).done(function (data) {
            if (data && data.code === 1) {
                //for (var i = 0; i < data.rows.length; i++) {
                //    if (data.rows[i]['createTime']) data.rows[i]['createTime'] = Yi.formatTime(data.rows[i]['createTime']);
                //}
                for(var i = 0; i < data.rows.length; i++){
                    var intervention = data.rows[i].intervention
                    if(intervention == 0){
                        data.rows[i]['isXitong'] = true;
                    }else if(intervention == 1){
                        data.rows[i]['isRengong'] = true;
                    }else if(intervention == 2){
                        data.rows[i]['isPingbi'] = true;
                    }
                }
                $tbody.append(Mustache.render($('#tpl-table').html(), data));
            }
        }).always(function(){
            $loading.hide();
        });
    }
});


